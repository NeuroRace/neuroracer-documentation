var searchData=
[
  ['read_5fconfig_5ffile',['read_config_file',['../namespaceneuroracer__ai_1_1utils_1_1config__handler.html#a5693e67ba97a7e1175abe76fee392079',1,'neuroracer_ai::utils::config_handler']]],
  ['relative',['relative',['../classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters.html#aad8ca91ad6eedac4935f184f57f8f8f0',1,'neuroracer_ai::processors::image::ImageCropParameters']]],
  ['relative_5fscaling',['relative_scaling',['../classneuroracer__ai_1_1processors_1_1image_1_1_image_resize_parameters.html#a5afef8e5fd2ca6cae51202d2762cf9bc',1,'neuroracer_ai::processors::image::ImageResizeParameters']]],
  ['resize_5fparameters',['resize_parameters',['../classneuroracer__ai_1_1processors_1_1image_1_1_image_processing_parameters.html#aa26290bdc94b350756839224eeafe7b3',1,'neuroracer_ai::processors::image::ImageProcessingParameters']]],
  ['right',['right',['../classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters.html#a02f9b82ada9ee8ff03f0b269cd670fd3',1,'neuroracer_ai::processors::image::ImageCropParameters']]]
];
