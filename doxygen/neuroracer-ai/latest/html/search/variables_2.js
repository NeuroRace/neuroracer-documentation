var searchData=
[
  ['camera_5fleft_5ftopic',['camera_left_topic',['../classneuroracer__ai_1_1suites_1_1snapshot_1_1_dual_view_snapshot_processor_suite.html#ac270e070f8e50d4ee2ebb39d776ed629',1,'neuroracer_ai::suites::snapshot::DualViewSnapshotProcessorSuite']]],
  ['camera_5fmsgs',['camera_msgs',['../classneuroracer__ai_1_1utils_1_1snapshot_1_1_image_snapshot.html#a66d15970375b7a109ad10951879f20a2',1,'neuroracer_ai::utils::snapshot::ImageSnapshot']]],
  ['camera_5fright_5ftopic',['camera_right_topic',['../classneuroracer__ai_1_1suites_1_1snapshot_1_1_dual_view_snapshot_processor_suite.html#ac9355d7b9645a77c02b34952da5e28dd',1,'neuroracer_ai::suites::snapshot::DualViewSnapshotProcessorSuite']]],
  ['camera_5ftopic',['camera_topic',['../classneuroracer__ai_1_1suites_1_1snapshot_1_1_single_view_snapshot_processor_suite.html#af12744ce142d963228b12d86bc30f26e',1,'neuroracer_ai::suites::snapshot::SingleViewSnapshotProcessorSuite']]],
  ['category',['category',['../namespaceneuroracer__ai_1_1models_1_1keras__base__backend.html#a2d40f0127e66c0ee67d3b5ba434c2c88',1,'neuroracer_ai::models::keras_base_backend']]],
  ['cpu',['CPU',['../namespaceneuroracer__ai_1_1models_1_1pytorch__base__backend.html#a87732d55f289cb1dce38eb06c8724ea8',1,'neuroracer_ai::models::pytorch_base_backend']]],
  ['crop_5fparameters',['crop_parameters',['../classneuroracer__ai_1_1processors_1_1image_1_1_image_processing_parameters.html#ad79b1883c1ed743ffc61a6d48c4fd5ae',1,'neuroracer_ai::processors::image::ImageProcessingParameters']]],
  ['cuda',['CUDA',['../namespaceneuroracer__ai_1_1models_1_1pytorch__base__backend.html#a22cee790371aa94e92dcd0e3d57122de',1,'neuroracer_ai::models::pytorch_base_backend']]],
  ['cv_5fdecode_5fas_5fis',['CV_DECODE_AS_IS',['../namespaceneuroracer__ai_1_1processors_1_1image.html#a5059de25b18383232c9ede728bb73fbb',1,'neuroracer_ai::processors::image']]],
  ['cv_5fload_5fimage_5fas_5fis',['CV_LOAD_IMAGE_AS_IS',['../namespaceneuroracer__ai_1_1utils_1_1snapshot.html#a3bf7bb4b604a0c6c27c44e96e8eeea41',1,'neuroracer_ai::utils::snapshot']]]
];
