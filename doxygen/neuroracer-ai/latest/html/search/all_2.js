var searchData=
[
  ['backend',['BACKEND',['../namespaceneuroracer__ai_1_1processors_1_1image.html#a99e6d5ef9b95efdb57725b916959c57f',1,'neuroracer_ai.processors.image.BACKEND()'],['../namespaceneuroracer__ai_1_1utils_1_1file__handler.html#aa0c631e4e4a1de1e2583d21dc03f6870',1,'neuroracer_ai.utils.file_handler.BACKEND()']]],
  ['backend_5ffactory_2epy',['backend_factory.py',['../backend__factory_8py.html',1,'']]],
  ['backend_5fsolver_2epy',['backend_solver.py',['../backend__solver_8py.html',1,'']]],
  ['backendfactory',['BackendFactory',['../classneuroracer__ai_1_1factories_1_1backend__factory_1_1_backend_factory.html',1,'neuroracer_ai::factories::backend_factory']]],
  ['bottom',['bottom',['../classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters.html#a9923f4f5bf0b8ff453cf53a18611b484',1,'neuroracer_ai::processors::image::ImageCropParameters']]],
  ['build',['build',['../classneuroracer__ai_1_1factories_1_1abstract_1_1_abstract_backend_factory.html#a18824751cc7ee7195311e1985f30d730',1,'neuroracer_ai.factories.abstract.AbstractBackendFactory.build()'],['../classneuroracer__ai_1_1factories_1_1backend__factory_1_1_backend_factory.html#a2b170ae4ec0878ebc885b2af8d75a331',1,'neuroracer_ai.factories.backend_factory.BackendFactory.build()']]]
];
