var classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters =
[
    [ "__init__", "classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters.html#a89ca68dbc82426b279d3867579235618", null ],
    [ "bottom", "classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters.html#a9923f4f5bf0b8ff453cf53a18611b484", null ],
    [ "left", "classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters.html#abd9f9975689aff3e19ef9211c98c7b0c", null ],
    [ "relative", "classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters.html#aad8ca91ad6eedac4935f184f57f8f8f0", null ],
    [ "right", "classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters.html#a02f9b82ada9ee8ff03f0b269cd670fd3", null ],
    [ "top", "classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters.html#aae33e6df0ee117eeed142042a9488bf8", null ]
];