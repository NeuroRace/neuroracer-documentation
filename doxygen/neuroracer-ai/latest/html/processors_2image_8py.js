var processors_2image_8py =
[
    [ "ImageResizeParameters", "classneuroracer__ai_1_1processors_1_1image_1_1_image_resize_parameters.html", "classneuroracer__ai_1_1processors_1_1image_1_1_image_resize_parameters" ],
    [ "ImageFlip", "classneuroracer__ai_1_1processors_1_1image_1_1_image_flip.html", null ],
    [ "ImageCropParameters", "classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters.html", "classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters" ],
    [ "ImageProcessingParameters", "classneuroracer__ai_1_1processors_1_1image_1_1_image_processing_parameters.html", "classneuroracer__ai_1_1processors_1_1image_1_1_image_processing_parameters" ],
    [ "ImageProcessor", "classneuroracer__ai_1_1processors_1_1image_1_1_image_processor.html", "classneuroracer__ai_1_1processors_1_1image_1_1_image_processor" ],
    [ "BACKEND", "processors_2image_8py.html#a99e6d5ef9b95efdb57725b916959c57f", null ],
    [ "CV_DECODE_AS_IS", "processors_2image_8py.html#a5059de25b18383232c9ede728bb73fbb", null ]
];