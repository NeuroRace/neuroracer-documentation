var namespaceneuroracer__ai_1_1suites =
[
    [ "abstract", "namespaceneuroracer__ai_1_1suites_1_1abstract.html", "namespaceneuroracer__ai_1_1suites_1_1abstract" ],
    [ "general", "namespaceneuroracer__ai_1_1suites_1_1general.html", "namespaceneuroracer__ai_1_1suites_1_1general" ],
    [ "image", "namespaceneuroracer__ai_1_1suites_1_1image.html", "namespaceneuroracer__ai_1_1suites_1_1image" ],
    [ "snapshot", "namespaceneuroracer__ai_1_1suites_1_1snapshot.html", "namespaceneuroracer__ai_1_1suites_1_1snapshot" ]
];