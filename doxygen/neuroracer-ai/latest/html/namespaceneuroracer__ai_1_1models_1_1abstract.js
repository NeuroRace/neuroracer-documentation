var namespaceneuroracer__ai_1_1models_1_1abstract =
[
    [ "AbstractModel", "classneuroracer__ai_1_1models_1_1abstract_1_1_abstract_model.html", "classneuroracer__ai_1_1models_1_1abstract_1_1_abstract_model" ],
    [ "Debuggable", "classneuroracer__ai_1_1models_1_1abstract_1_1_debuggable.html", "classneuroracer__ai_1_1models_1_1abstract_1_1_debuggable" ],
    [ "Trainable", "classneuroracer__ai_1_1models_1_1abstract_1_1_trainable.html", "classneuroracer__ai_1_1models_1_1abstract_1_1_trainable" ]
];