var classneuroracer__ai_1_1processors_1_1image_1_1_image_resize_parameters =
[
    [ "__init__", "classneuroracer__ai_1_1processors_1_1image_1_1_image_resize_parameters.html#aab927d2e499b47d918744c9b7572c163", null ],
    [ "interpolation_method", "classneuroracer__ai_1_1processors_1_1image_1_1_image_resize_parameters.html#aca05a0178a3841209fe77ba51c6be0db", null ],
    [ "relative_scaling", "classneuroracer__ai_1_1processors_1_1image_1_1_image_resize_parameters.html#a5afef8e5fd2ca6cae51202d2762cf9bc", null ],
    [ "x_scaling", "classneuroracer__ai_1_1processors_1_1image_1_1_image_resize_parameters.html#aa62d9ecc34f24f737ec1b90eab4516c7", null ],
    [ "y_scaling", "classneuroracer__ai_1_1processors_1_1image_1_1_image_resize_parameters.html#ad114dba5278c5db8d831eb5f7779c306", null ]
];