var hierarchy =
[
    [ "ABC", null, [
      [ "neuroracer_ai.factories.abstract.AbstractBackendFactory", "classneuroracer__ai_1_1factories_1_1abstract_1_1_abstract_backend_factory.html", [
        [ "neuroracer_ai.factories.backend_factory.BackendFactory", "classneuroracer__ai_1_1factories_1_1backend__factory_1_1_backend_factory.html", null ]
      ] ],
      [ "neuroracer_ai.models.abstract.AbstractModel", "classneuroracer__ai_1_1models_1_1abstract_1_1_abstract_model.html", [
        [ "neuroracer_ai.models.keras_base_backend.KerasBaseModel", "classneuroracer__ai_1_1models_1_1keras__base__backend_1_1_keras_base_model.html", null ],
        [ "neuroracer_ai.models.pytorch_base_backend.PyTorchBaseModel", "classneuroracer__ai_1_1models_1_1pytorch__base__backend_1_1_py_torch_base_model.html", null ]
      ] ],
      [ "neuroracer_ai.models.abstract.Debuggable", "classneuroracer__ai_1_1models_1_1abstract_1_1_debuggable.html", [
        [ "neuroracer_ai.models.keras_base_backend.KerasBaseModel", "classneuroracer__ai_1_1models_1_1keras__base__backend_1_1_keras_base_model.html", null ],
        [ "neuroracer_ai.models.pytorch_base_backend.PyTorchBaseModel", "classneuroracer__ai_1_1models_1_1pytorch__base__backend_1_1_py_torch_base_model.html", null ]
      ] ],
      [ "neuroracer_ai.models.abstract.Trainable", "classneuroracer__ai_1_1models_1_1abstract_1_1_trainable.html", null ],
      [ "neuroracer_ai.processors.abstract.AbstractProcessor", "classneuroracer__ai_1_1processors_1_1abstract_1_1_abstract_processor.html", [
        [ "neuroracer_ai.processors.image.ImageProcessor", "classneuroracer__ai_1_1processors_1_1image_1_1_image_processor.html", null ],
        [ "neuroracer_ai.suites.abstract.AbstractProcessorSuite", "classneuroracer__ai_1_1suites_1_1abstract_1_1_abstract_processor_suite.html", [
          [ "neuroracer_ai.suites.general.EmptyProcessorSuite", "classneuroracer__ai_1_1suites_1_1general_1_1_empty_processor_suite.html", null ],
          [ "neuroracer_ai.suites.general.ManualMappingProcessorSuite", "classneuroracer__ai_1_1suites_1_1general_1_1_manual_mapping_processor_suite.html", null ],
          [ "neuroracer_ai.suites.image.Image2DProcessorSuite", "classneuroracer__ai_1_1suites_1_1image_1_1_image2_d_processor_suite.html", null ],
          [ "neuroracer_ai.suites.snapshot.SnapshotProcessorSuite", "classneuroracer__ai_1_1suites_1_1snapshot_1_1_snapshot_processor_suite.html", [
            [ "neuroracer_ai.suites.snapshot.DualViewSnapshotProcessorSuite", "classneuroracer__ai_1_1suites_1_1snapshot_1_1_dual_view_snapshot_processor_suite.html", null ],
            [ "neuroracer_ai.suites.snapshot.SingleViewSnapshotProcessorSuite", "classneuroracer__ai_1_1suites_1_1snapshot_1_1_single_view_snapshot_processor_suite.html", null ]
          ] ]
        ] ]
      ] ],
      [ "neuroracer_ai.suites.abstract.AbstractProcessorSuite", "classneuroracer__ai_1_1suites_1_1abstract_1_1_abstract_processor_suite.html", null ],
      [ "neuroracer_ai.utils.abstract.AbstractGenerator", "classneuroracer__ai_1_1utils_1_1abstract_1_1_abstract_generator.html", null ]
    ] ],
    [ "neuroracer_ai.utils.snapshot.AbstractSnapshot", "classneuroracer__ai_1_1utils_1_1snapshot_1_1_abstract_snapshot.html", [
      [ "neuroracer_ai.utils.snapshot.ImageSnapshot", "classneuroracer__ai_1_1utils_1_1snapshot_1_1_image_snapshot.html", null ]
    ] ],
    [ "neuroracer_ai.ai.AI", "classneuroracer__ai_1_1ai_1_1_a_i.html", null ],
    [ "neuroracer_ai.factories.abstract.EBackends", "classneuroracer__ai_1_1factories_1_1abstract_1_1_e_backends.html", null ],
    [ "neuroracer_ai.processors.image.ImageCropParameters", "classneuroracer__ai_1_1processors_1_1image_1_1_image_crop_parameters.html", null ],
    [ "neuroracer_ai.processors.image.ImageProcessingParameters", "classneuroracer__ai_1_1processors_1_1image_1_1_image_processing_parameters.html", null ],
    [ "neuroracer_ai.processors.image.ImageResizeParameters", "classneuroracer__ai_1_1processors_1_1image_1_1_image_resize_parameters.html", null ],
    [ "neuroracer_ai.utils.shared_value_dict.SharedValueDict", "classneuroracer__ai_1_1utils_1_1shared__value__dict_1_1_shared_value_dict.html", null ],
    [ "Enum", null, [
      [ "neuroracer_ai.processors.image.ImageFlip", "classneuroracer__ai_1_1processors_1_1image_1_1_image_flip.html", null ]
    ] ]
];