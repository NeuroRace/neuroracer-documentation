var dir_3fa33a397f10d4332d1ba272e753409d =
[
    [ "__init__.py", "suites_2____init_____8py.html", "suites_2____init_____8py" ],
    [ "abstract.py", "suites_2abstract_8py.html", "suites_2abstract_8py" ],
    [ "general.py", "general_8py.html", [
      [ "EmptyProcessorSuite", "classneuroracer__ai_1_1suites_1_1general_1_1_empty_processor_suite.html", "classneuroracer__ai_1_1suites_1_1general_1_1_empty_processor_suite" ],
      [ "ManualMappingProcessorSuite", "classneuroracer__ai_1_1suites_1_1general_1_1_manual_mapping_processor_suite.html", "classneuroracer__ai_1_1suites_1_1general_1_1_manual_mapping_processor_suite" ]
    ] ],
    [ "image.py", "suites_2image_8py.html", "suites_2image_8py" ],
    [ "snapshot.py", "suites_2snapshot_8py.html", [
      [ "SnapshotProcessorSuite", "classneuroracer__ai_1_1suites_1_1snapshot_1_1_snapshot_processor_suite.html", "classneuroracer__ai_1_1suites_1_1snapshot_1_1_snapshot_processor_suite" ],
      [ "SingleViewSnapshotProcessorSuite", "classneuroracer__ai_1_1suites_1_1snapshot_1_1_single_view_snapshot_processor_suite.html", "classneuroracer__ai_1_1suites_1_1snapshot_1_1_single_view_snapshot_processor_suite" ],
      [ "DualViewSnapshotProcessorSuite", "classneuroracer__ai_1_1suites_1_1snapshot_1_1_dual_view_snapshot_processor_suite.html", "classneuroracer__ai_1_1suites_1_1snapshot_1_1_dual_view_snapshot_processor_suite" ]
    ] ]
];