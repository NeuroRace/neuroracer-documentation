var namespaceneuroracer__ai_1_1models =
[
    [ "abstract", "namespaceneuroracer__ai_1_1models_1_1abstract.html", "namespaceneuroracer__ai_1_1models_1_1abstract" ],
    [ "keras_base_backend", "namespaceneuroracer__ai_1_1models_1_1keras__base__backend.html", "namespaceneuroracer__ai_1_1models_1_1keras__base__backend" ],
    [ "pytorch_base_backend", "namespaceneuroracer__ai_1_1models_1_1pytorch__base__backend.html", "namespaceneuroracer__ai_1_1models_1_1pytorch__base__backend" ]
];