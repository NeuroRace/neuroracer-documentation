var classneuroracer__ai__sl_1_1ai_1_1_train_parameters =
[
    [ "__init__", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#ad9085fd25aba509e69b20e64359ad9d3", null ],
    [ "architecture", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#ab0d8b12ec0bd968ad902cdabc8d715df", null ],
    [ "batch_size", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#aca73c48333c287eedb063e299a350b5e", null ],
    [ "checkpoint_dir", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#a7e35445138f417773dd79c95963bf405", null ],
    [ "data_processor", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#a8b73d79f89b18510c13f155e13cf9d4f", null ],
    [ "processor_suite", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#aa32fc79373c28549c46374c04a3260dc", null ],
    [ "shuffle", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#ae67feecc43b73abe44f654af199628ab", null ],
    [ "steps_per_epoch", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#a36e58ea064d27fac130e877a4e004c6c", null ],
    [ "train_data_generator", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#a8da5b432070d56ee7b594617ac952b1e", null ],
    [ "use_builtin_pipeline", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#a7a0bde1342e1c87dabaedc03da01d6b7", null ],
    [ "use_checkpoints", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#a222da6cb42b168d2a182c1f58ba0c647", null ],
    [ "validation_data_generator", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#a50eee89e2858c5c00f9fc057db319c8a", null ],
    [ "validation_steps", "classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#a53e8bcf18b08372a1998883de1953d6e", null ]
];