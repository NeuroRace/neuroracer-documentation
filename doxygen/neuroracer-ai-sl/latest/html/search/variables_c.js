var searchData=
[
  ['shuffle',['shuffle',['../classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#ae67feecc43b73abe44f654af199628ab',1,'neuroracer_ai_sl.ai.TrainParameters.shuffle()'],['../classneuroracer__ai__sl_1_1utils_1_1bag__reader_1_1_rosbag_reader.html#a56d69e2aaecc67751ababa4c47e85551',1,'neuroracer_ai_sl.utils.bag_reader.RosbagReader.shuffle()'],['../classneuroracer__ai__sl_1_1utils_1_1data__reader_1_1_extracted_data_reader.html#a29622d5c4d38e64e5f093c60bade8a85',1,'neuroracer_ai_sl.utils.data_reader.ExtractedDataReader.shuffle()']]],
  ['steps_5fper_5fepoch',['steps_per_epoch',['../classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#a36e58ea064d27fac130e877a4e004c6c',1,'neuroracer_ai_sl::ai::TrainParameters']]]
];
