var searchData=
[
  ['train',['train',['../classneuroracer__ai__sl_1_1ai_1_1_a_i.html#a2fa917dc0bab662c83ead448057f64e7',1,'neuroracer_ai_sl.ai.AI.train()'],['../classneuroracer__ai__sl_1_1models_1_1keras__backend__sl_1_1_keras_model_s_l.html#a5589e1ee908f3265ad2a00f7ed863ade',1,'neuroracer_ai_sl.models.keras_backend_sl.KerasModelSL.train()'],['../classneuroracer__ai__sl_1_1models_1_1pytorch__backend__sl_1_1_py_torch_model_s_l.html#a013bb4f8bfbbbd87c30845b651f3244e',1,'neuroracer_ai_sl.models.pytorch_backend_sl.PyTorchModelSL.train()']]],
  ['transform_5ftask_5fdicts',['transform_task_dicts',['../namespaceneuroracer__ai__sl_1_1cli_1_1debug.html#a32c47ce5219bf4c1ca46a73ac04aca05',1,'neuroracer_ai_sl::cli::debug']]]
];
