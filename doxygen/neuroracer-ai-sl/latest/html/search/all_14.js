var searchData=
[
  ['validation_5fdata_5fgenerator',['validation_data_generator',['../classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#a50eee89e2858c5c00f9fc057db319c8a',1,'neuroracer_ai_sl::ai::TrainParameters']]],
  ['validation_5fsteps',['validation_steps',['../classneuroracer__ai__sl_1_1ai_1_1_train_parameters.html#a53e8bcf18b08372a1998883de1953d6e',1,'neuroracer_ai_sl::ai::TrainParameters']]],
  ['visualizeactivationtask',['VisualizeActivationTask',['../classneuroracer__ai__sl_1_1cli_1_1debug_1_1_visualize_activation_task.html',1,'neuroracer_ai_sl::cli::debug']]],
  ['visualizesaliencytask',['VisualizeSaliencyTask',['../classneuroracer__ai__sl_1_1cli_1_1debug_1_1_visualize_saliency_task.html',1,'neuroracer_ai_sl::cli::debug']]]
];
