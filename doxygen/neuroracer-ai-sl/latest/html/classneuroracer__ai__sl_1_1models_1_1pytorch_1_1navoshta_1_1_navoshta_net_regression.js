var classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_regression =
[
    [ "__init__", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_regression.html#a95de69119607d8076003ca296ab4c6c6", null ],
    [ "forward", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_regression.html#a73c381e071f7ef705ca0d9e4938ebb2f", null ],
    [ "get_loss_function", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_regression.html#abf92acaa7ff7ebe80120badad383f93d", null ],
    [ "get_optimizer", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_regression.html#a55baccbda2055d0b040449cf86e87763", null ],
    [ "layer_1", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_regression.html#ab5c6ab213df94d76934c2ce06f33b66f", null ],
    [ "layer_2", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_regression.html#a89c68d006b3dd3dcbb9b6f3650655c12", null ],
    [ "layer_3", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_regression.html#a8383940dd1999e485b4eb758335a92ab", null ],
    [ "layer_4", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_regression.html#a835127136dad27fad057bdb1b77b12f9", null ],
    [ "layer_5", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_regression.html#ae7f1ef35aab170bb538a3b5e3c370fa7", null ],
    [ "layer_6", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_regression.html#a4495412cb6a1e8948ccbcbf4881b9785", null ],
    [ "layer_7", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_regression.html#ab420e229520117521e36ec1038e72e21", null ]
];