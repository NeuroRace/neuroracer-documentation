var dir_48cf2b43f049a60009865102e1eeccf4 =
[
    [ "keras", "dir_544a6ccec03d5993684b99a8b73c524b.html", "dir_544a6ccec03d5993684b99a8b73c524b" ],
    [ "pytorch", "dir_915675b61eab76883d8cb8eb8bc99caa.html", "dir_915675b61eab76883d8cb8eb8bc99caa" ],
    [ "__init__.py", "models_2____init_____8py.html", "models_2____init_____8py" ],
    [ "keras_backend_sl.py", "keras__backend__sl_8py.html", "keras__backend__sl_8py" ],
    [ "pytorch_backend_sl.py", "pytorch__backend__sl_8py.html", [
      [ "DatasetSL", "classneuroracer__ai__sl_1_1models_1_1pytorch__backend__sl_1_1_dataset_s_l.html", "classneuroracer__ai__sl_1_1models_1_1pytorch__backend__sl_1_1_dataset_s_l" ],
      [ "PyTorchModelSL", "classneuroracer__ai__sl_1_1models_1_1pytorch__backend__sl_1_1_py_torch_model_s_l.html", "classneuroracer__ai__sl_1_1models_1_1pytorch__backend__sl_1_1_py_torch_model_s_l" ],
      [ "PyTorchArchitectures", "classneuroracer__ai__sl_1_1models_1_1pytorch__backend__sl_1_1_py_torch_architectures.html", "classneuroracer__ai__sl_1_1models_1_1pytorch__backend__sl_1_1_py_torch_architectures" ]
    ] ]
];