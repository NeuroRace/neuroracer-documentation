var classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_classification =
[
    [ "__init__", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_classification.html#a547fd8d761b48e9956cd644e0a5aa6be", null ],
    [ "forward", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_classification.html#a86936eb2e78d8a11aefaa19a2b85573a", null ],
    [ "get_loss_function", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_classification.html#a5e091f342e83172d63920e9e19bfa5e8", null ],
    [ "get_optimizer", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_classification.html#a1d41ec2c36862de39e615445ff95675b", null ],
    [ "layer_1", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_classification.html#a73595ec3f9c8623bf3bfd828ae6266ff", null ],
    [ "layer_2", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_classification.html#a82f6d7c64dc4b733a60e47ca9529a597", null ],
    [ "layer_3", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_classification.html#a7e641e9c53b0955723529b409b1d638d", null ],
    [ "layer_4", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_classification.html#a0419860e9ac3a1dc00b27007e9ec3794", null ],
    [ "layer_5", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_classification.html#ae537efb5aad4708355753ecb298517b9", null ],
    [ "layer_6", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_classification.html#ae9a76eb1a2871df714b0749482062a91", null ],
    [ "layer_7", "classneuroracer__ai__sl_1_1models_1_1pytorch_1_1navoshta_1_1_navoshta_net_classification.html#a8422e52e63fedfb86d768133747c5bb7", null ]
];