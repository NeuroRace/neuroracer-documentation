var namespaces_dup =
[
    [ "neuroracer_common", null, [
      [ "devices", null, [
        [ "gamepad", "namespaceneuroracer__common_1_1devices_1_1gamepad.html", null ]
      ] ],
      [ "subscribers", "namespaceneuroracer__common_1_1subscribers.html", null ],
      [ "third_party", null, [
        [ "profilehooks", "namespaceneuroracer__common_1_1third__party_1_1profilehooks.html", null ]
      ] ],
      [ "type_solving", "namespaceneuroracer__common_1_1type__solving.html", null ],
      [ "version_solver", "namespaceneuroracer__common_1_1version__solver.html", null ]
    ] ]
];