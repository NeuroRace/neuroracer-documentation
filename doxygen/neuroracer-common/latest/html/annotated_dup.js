var annotated_dup =
[
    [ "neuroracer_common", null, [
      [ "actions", null, [
        [ "ActionTransformer", "classneuroracer__common_1_1actions_1_1_action_transformer.html", "classneuroracer__common_1_1actions_1_1_action_transformer" ],
        [ "ActionTransformerRacerTwist", "classneuroracer__common_1_1actions_1_1_action_transformer_racer_twist.html", "classneuroracer__common_1_1actions_1_1_action_transformer_racer_twist" ]
      ] ],
      [ "ai", null, [
        [ "AIWrapper", "classneuroracer__common_1_1ai_1_1_a_i_wrapper.html", "classneuroracer__common_1_1ai_1_1_a_i_wrapper" ]
      ] ],
      [ "collector", null, [
        [ "Collector", "classneuroracer__common_1_1collector_1_1_collector.html", "classneuroracer__common_1_1collector_1_1_collector" ]
      ] ],
      [ "devices", null, [
        [ "gamepad", "namespaceneuroracer__common_1_1devices_1_1gamepad.html", "namespaceneuroracer__common_1_1devices_1_1gamepad" ]
      ] ],
      [ "errors", null, [
        [ "ConfigError", "classneuroracer__common_1_1errors_1_1_config_error.html", null ],
        [ "MissingFieldConfigError", "classneuroracer__common_1_1errors_1_1_missing_field_config_error.html", null ],
        [ "WrongTypeConfigError", "classneuroracer__common_1_1errors_1_1_wrong_type_config_error.html", null ]
      ] ],
      [ "operator", null, [
        [ "Operator", "classneuroracer__common_1_1operator_1_1_operator.html", "classneuroracer__common_1_1operator_1_1_operator" ]
      ] ],
      [ "subscribers", "namespaceneuroracer__common_1_1subscribers.html", "namespaceneuroracer__common_1_1subscribers" ],
      [ "third_party", null, [
        [ "profilehooks", "namespaceneuroracer__common_1_1third__party_1_1profilehooks.html", "namespaceneuroracer__common_1_1third__party_1_1profilehooks" ]
      ] ]
    ] ]
];